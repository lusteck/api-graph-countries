import React, { useEffect } from 'react';
import useReactRouter from "use-react-router";
import { connect, useSelector, useDispatch } from 'react-redux';
import { Link } from "react-router-dom";

import { useStyles } from './styles';
import Form from '../Form';
import { selectAll } from '../../store/reducers/selectors';

import {
    Card,
    CardActionArea,
    CardContent,
    CardMedia,
    CardActions,
    Button,
    Typography
} from '@material-ui/core';

const DetalhePais = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const result = useSelector(selectAll);
    const { match } = useReactRouter();

    useEffect(() => {
        async function obterPais() {
            const response = await fetch('https://countries-274616.ew.r.appspot.com', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    query: `
                        query {
                            Country {
                                name
                                capital
                                area
                                population
                                numericCode
                                flag {
                                    svgFile
                                }
                                topLevelDomains {
                                    name
                                }
                            }
                        }
                    `
                })
            });

            const data = await response.json();

            return data;
        };

        obterPais().then(data => {
            var pais = data.data.Country.filter(item => {
                if (item.numericCode && item.numericCode.indexOf(match.params.id) > -1) return item
            });

            dispatch({
                type: 'INIT_PAIS',
                pais: pais
            });
        });
    }, []);

    return (
        <div className={classes.container}>
            {result.pais.map((pais, index) =>
                <Card key={'pais' + index} className={classes.card}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            component="img"
                            alt={"Bandeira de " + pais.name}
                            image={pais.flag.svgFile}
                            title={"Mais detalhes " + pais.name}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {pais.name}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Capital: {pais.capital}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Area: {pais.area} km²
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                População: {pais.population}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Domínio de topo: {pais.topLevelDomains[0].name}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button
                            size="small"
                            color="primary"
                            onClick={() => dispatch({
                                type: 'OPEN'
                            })}
                        >
                            Editar
                        </Button>

                        <Button
                            size="small"
                            color="primary"
                        >
                            <Link to="/">Voltar</Link>
                        </Button>
                    </CardActions>
                </Card>
            )}

            {result.openhideform && (
                <Form />
            )}
        </div>
    );
}

function mapStateToProps(state) {
    return {
        openhideform: state,
        pais: state
    }
}

export default connect(state => (mapStateToProps))(DetalhePais);
