import {
    makeStyles
} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    form: {
        display: "block"
    },
    inputForm: {
        width: "100%",
        boxSizing: "border-box",
        margin: "10px 0",
        padding: "10px",
        border: "solid 1px #ccc",
        display: "block"
    },
    titleForm: {
        marginTop: "30px"
    },
    btnSalvar: {
        marginTop: "20px"
    },
    btnCancelar: {
        marginTop: "20px",
        marginLeft: "15px",
        backgroundColor: "rgba(63, 81, 181, 0.1)"
    }
});
