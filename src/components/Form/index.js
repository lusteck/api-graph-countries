import React from 'react';
import { Form as FormUndorm } from '@unform/web';
import Input from '../Form/Input';
import { useDispatch, useSelector } from 'react-redux';
import { selectAll } from '../../store/reducers/selectors';

import {
    Button,
    Typography
} from '@material-ui/core';

import { useStyles } from './styles';

const Form = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const result = useSelector(selectAll);

    function handleSubmit(data) {
        var pais = result.pais[0];
        pais.name = data.nome;
        pais.capital = data.capital;
        pais.area = data.area;
        pais.population = data.populacao;

        dispatch({
            type: 'SAVE',
            pais: [pais]
        })
    }

    return (
        <FormUndorm className={classes.form} onSubmit={handleSubmit} initialData={{
            nome: result.pais[0].name,
            capital: result.pais[0].capital,
            area: result.pais[0].area,
            populacao: result.pais[0].population
        }}>
            <Typography variant="h6" color="textPrimary" className={classes.titleForm} component="h3" >
                Edite as informações desse país:
            </Typography>

            <Input required name="nome" placeholder="Nome" id="nome" type="text" className={classes.inputForm} />
            <Input required name="capital" placeholder="Capital" id="capital" type="text" className={classes.inputForm} />
            <Input required name="area" placeholder="Area" id="area" type="text" className={classes.inputForm} />
            <Input required name="populacao" placeholder="População" id="populacao" type="text" className={classes.inputForm} />

            <Button
                type="submit"
                size="medium"
                variant="outlined"
                color="primary"
                className={classes.btnSalvar}
            >
                Salvar
            </Button>

            <Button
                size="medium"
                variant="outlined"
                color="primary"
                className={classes.btnCancelar}
                onClick={() => dispatch({
                    type: 'CLOSE'
                })}
            >
                Cancelar
            </Button>
        </FormUndorm>
    );
}

export default Form;
