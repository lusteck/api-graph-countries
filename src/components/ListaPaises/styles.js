import {
    makeStyles
} from '@material-ui/core/styles';

export const useStyles = makeStyles({
    container: {
        width: "100%",
        maxWidth: "500px",
        margin: "20px auto",
        boxShadow: "2px 2px 4px rgba(0,0,0,0.4)",
        padding: "20px",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between"
    },
    card: {
        width: "47%",
        marginBottom: "30px"
    },
    media: {
        height: 140,
    },
});
