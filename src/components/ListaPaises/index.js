import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

import {
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    Button,
    Typography
} from '@material-ui/core';

import { useStyles } from './styles';

const ListaPaises = () => {
    const classes = useStyles();
    const [paises, setPaises] = useState({ Country: [] });

    useEffect(() => {
        async function obterTodos() {
            const response = await fetch('https://countries-274616.ew.r.appspot.com', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    query: `
                        query {
                            Country {
                                name
                                capital
                                numericCode
                                flag {
                                    svgFile
                                }
                            }
                        }

                    `
                })
            })
            .then(res => res.json());

            setPaises(response.data);
        }

        obterTodos();
    }, []);

    return (
        <div className={classes.container}>
            {paises.Country.map((pais, index) =>
                <Card key={'pais-'+index} className={classes.card}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            component="img"
                            alt={"Bandeira de " + pais.name}
                            image={pais.flag.svgFile}
                            title={"Mais detalhes " + pais.name}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {pais.name}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                Capital: {pais.capital}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button size="small" color="primary"><Link to={"/detalhe/"+pais.numericCode}>Ver mais Detalhes</Link></Button>
                    </CardActions>
                </Card>
            )}
        </div>
    );
}

export default ListaPaises;
