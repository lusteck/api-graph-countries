import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import DetalhePais from './components/DetalhePais';
import ListaPaises from './components/ListaPaises';

function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" component={ListaPaises} exact />
                <Route path="/detalhe/:id" component={DetalhePais} />
                <Route component={() => <div><h1>Página não encontrada. Erro 404</h1></div>} />
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
