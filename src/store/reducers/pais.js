const STATE = [{
    area: 0,
    capital: "Nome",
    flag: { svgFile: "https://noimage.jpg" },
    name: "Nome",
    numericCode: "0",
    population: 0,
    topLevelDomains: [{
        name: ".a"
    }]
}]

export default function pais(state = STATE, action) {
    switch (action.type) {
        case 'INIT_PAIS':
            return action.pais

        case 'SAVE':
            return action.pais;

        default:
            return state
    }
}
