import {combineReducers} from 'redux';

import openhideform from './openhideform';
import pais from './pais';

export default combineReducers({
    openhideform,
    pais
});
