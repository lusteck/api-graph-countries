export default function reducer(state = false, action) {
    switch (action.type) {
        case 'OPEN':
            return true;

        case 'CLOSE':
            return false;

        case 'SAVE':
            return false;

        default:
            return state;
    }
}
